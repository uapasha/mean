import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from './core/services/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    private router: Router,
    private authService: AuthorizationService,
  ) {}
  title = 'task scheduler';
  get isLoggedIn() {
    return this.authService.isAuthorized();
  }

  get userName() {
    return this.authService.getUserName();
  }
  navigate(url) {
    this.router.navigate([url]);
  }
  logout() {
    this.authService.eraseAuthInformation();
    this.router.navigate(['/']);
  }
}
