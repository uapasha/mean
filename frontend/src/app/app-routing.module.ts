import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './pages/login/login-page.component';
import { TaskCreatePage } from './pages/task-create/task-create-page.component';
import { TaskEditPage } from './pages/task-edit/task-edit-page.component';
import { TaskListPage } from './pages/task-list/task-list-page.component';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  { path: 'create', component: TaskCreatePage, canActivate: [AuthGuard] },
  { path: 'edit/:id', component: TaskEditPage, canActivate: [AuthGuard] },
  { path: 'list', component: TaskListPage, canActivate: [AuthGuard] },
  { path: 'login', component: LoginPage },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
