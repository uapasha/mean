import { Component, OnInit } from '@angular/core';
import { Task, TasksService } from '../../core/services/tasks.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list-page.component.html',
  styleUrls: ['./task-list-page.component.scss'],
})
export class TaskListPage implements OnInit {
  tasks: Task[];

  constructor(
    private tasksService: TasksService,
    private router: Router,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.fetchTasks();
  }

  editTask(id: string) {
    this.router.navigate([`/edit/${id}`]);
  }

  addTask() {
    this.router.navigate(['create']);
  }

  removeTask(id: string) {
    this.tasksService.remove(id).subscribe(() => {
      this.fetchTasks();
      this.notificationService.notify('Task removed');
    });
  }

  private fetchTasks() {
    this.tasksService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
    });
  }
}
