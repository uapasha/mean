import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from '../../core/services/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPage implements OnInit {
  userForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthorizationService) {}

  ngOnInit() {
    this.userForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(7)]],
    });
  }

  onSubmit() {
    // eslint-disable-next-line no-console
    this.authService.authorizeWithPassword(this.userForm.value).subscribe();
  }
}
