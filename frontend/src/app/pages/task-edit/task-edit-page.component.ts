import { Component, OnInit } from '@angular/core';
import { Task, TasksService } from '../../core/services/tasks.service';
import { ActivatedRoute, Router } from '@angular/router';
import { flatMap } from 'rxjs/operators';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit-page.component.html',
  styleUrls: ['./task-edit-page.component.scss'],
})
export class TaskEditPage implements OnInit {
  task: Task;

  constructor(
    private taskService: TasksService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap
      .pipe(
        flatMap(params => {
          const id = params.get('id');
          return this.taskService.getTask(id);
        }),
      )
      .subscribe(task => {
        this.task = task;
      });
  }

  onSave(task) {
    this.taskService.update(task).subscribe(updatedTask => {
      this.notificationService.notify('Task Updated');
      this.task = updatedTask;
    });
  }
}
