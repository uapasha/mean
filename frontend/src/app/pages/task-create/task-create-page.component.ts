import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../core/services/tasks.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tasc-create',
  templateUrl: './task-create-page.component.html',
  styleUrls: ['./task-create-page.component.scss'],
})
export class TaskCreatePage implements OnInit {
  constructor(
    private taskService: TasksService,
    private notificationService: NotificationService,
    private router: Router,
  ) {}

  ngOnInit() {}

  onSave(task) {
    this.taskService.add(task).subscribe(() => {
      this.notificationService.notify('Task Created!');
      this.router.navigate(['/list']);
    });
  }
}
