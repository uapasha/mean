import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Task } from '../../../core/services/tasks.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Location,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
  providers: [
    Location,
    { provide: LocationStrategy, useClass: PathLocationStrategy },
  ],
})
export class TaskComponent implements OnInit {
  @Input() task: Task;
  @Output() save = new EventEmitter<Task>();
  taskForm: FormGroup;

  constructor(private fb: FormBuilder, private location: Location) {}

  ngOnInit() {
    this.taskForm = this.fb.group({
      _id: [this.task && this.task._id],
      description: [
        this.task && this.task.description,
        [Validators.required, Validators.minLength(10)],
      ],
      dueDate: [
        this.task && this.task.dueDate.toISOString().substr(0, 10),
        [Validators.required],
      ],
    });
  }

  onSubmit() {
    const dueDate = new Date(this.taskForm.value.dueDate);
    this.save.emit({
      ...this.taskForm.value,
      dueDate,
    });
  }

  goBack() {
    this.location.back();
  }
}
