import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  constructor(private snackBar: MatSnackBar) {}

  notify(title: string, message?: string) {
    this.snackBar.open(title, message, {
      duration: 10000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }
}
