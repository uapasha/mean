import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Router } from '@angular/router';
import { StorageKey, StorageService } from './storage.service';
import { NotificationService } from './notification.service';

export interface AuthData {
  login: string;
  password: string;
}

export interface AuthResponse {
  userName: string;
  token: string;
  userId: string;
}

export interface LoggedInUser {
  userName: string;
  userId: string;
}

@Injectable({ providedIn: 'root' })
export class AuthorizationService {
  private isAuthorizedSubject = new BehaviorSubject<boolean>(false);
  private authToken: string;
  private user: LoggedInUser;

  private authRootUrl = '/user/login';

  redirectUrl: string;

  constructor(
    private httpClient: HttpClient,
    private storageService: StorageService,
    private router: Router,
    private notificationService: NotificationService,
  ) {
    this.authToken = this.storageService.getItem(StorageKey.AUTH_TOKEN);
    this.user =
      JSON.parse(this.storageService.getItem(StorageKey.USER)) || null;

    if (this.authToken) {
      this.isAuthorizedSubject.next(true);
    }
  }

  isAuthorized(): Observable<boolean> {
    return this.isAuthorizedSubject.asObservable();
  }

  authorizeWithPassword(authData: AuthData): Observable<AuthResponse> {
    return this.httpClient.post<AuthResponse>(this.authRootUrl, authData).pipe(
      tap(this.saveUser),
      tap(this.saveAuthToken),
      tap(this.handleLoginResponse),
    );
  }

  eraseAuthInformation() {
    this.eraseAuthToken();
    this.eraseCurrentUserInfo();
  }

  getAuthToken(): string | null {
    return this.authToken;
  }

  getUserId(): string | null {
    return this.user && this.user.userId;
  }

  getUserName(): string | null {
    return this.user && this.user.userName;
  }

  private saveAuthToken = ({ token }: AuthResponse) => {
    if (token) {
      this.storageService.setItem(StorageKey.AUTH_TOKEN, token);
      this.authToken = token;
      this.isAuthorizedSubject.next(true);
    }
  };

  private saveUser = ({ userId, userName }: AuthResponse) => {
    if (userId) {
      this.storageService.setItem(
        StorageKey.USER,
        JSON.stringify({ userName, userId }),
      );
      this.user = { userId, userName };
    }
  };

  private eraseAuthToken() {
    this.storageService.removeItem(StorageKey.AUTH_TOKEN);
    this.authToken = null;
    this.isAuthorizedSubject.next(false);
  }

  private eraseCurrentUserInfo() {
    this.storageService.removeItem(StorageKey.USER);
    this.user = null;
  }

  private handleLoginResponse = ({ token }: AuthResponse) => {
    const redirectUrl = this.redirectUrl;
    this.redirectUrl = null;
    if (token) {
      this.notificationService.notify('User is logged in');
      this.router.navigate([redirectUrl || '/list']);
    } else {
      this.notificationService.notify('User failed to log in');
    }

    return token;
  };
}
