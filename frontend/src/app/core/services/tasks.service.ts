import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthorizationService } from './authorization.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Task {
  _id: string;
  description: string;
  dueDate: Date;
}

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  uri = '/task';
  constructor(
    private http: HttpClient,
    private authService: AuthorizationService,
  ) {}
  getTasks(): Observable<Task[]> {
    return this.http
      .get<Task[]>(`${this.uri}/${this.authService.getUserId()}`)
      .pipe(map(tasksData => tasksData.map(this.mapObject)));
  }

  getTask(id: string): Observable<Task> {
    return this.http
      .get<Task>(`${this.uri}/single/${id}`)
      .pipe(map(this.mapObject));
  }

  update(task: Task): Observable<Task> {
    return this.http.post<Task>(this.uri, task);
  }

  add(task: Task): Observable<Task> {
    return this.http.put<Task>(this.uri, {
      ...task,
      userId: this.authService.getUserId(),
    });
  }

  remove(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.uri}/${id}`);
  }

  private mapObject(task: any): Task {
    return { ...task, dueDate: new Date(task.dueDate) };
  }
}
