import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { AuthorizationService } from '../services/authorization.service';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {
  private readonly AUTH_HEADER = 'Authorization';

  constructor(private authService: AuthorizationService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const token = this.authService.getAuthToken();
    let request = req;
    if (!isNullOrUndefined(token)) {
      request = req.clone({
        headers: req.headers.append(this.AUTH_HEADER, token),
      });
    }
    return next.handle(request);
  }
}
