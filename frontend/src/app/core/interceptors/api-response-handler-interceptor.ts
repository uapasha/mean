import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';
import { AuthorizationService } from '../services/authorization.service';

@Injectable()
export class ApiResponseHandlerInterceptor implements HttpInterceptor {
  constructor(
    private notificationService: NotificationService,
    private router: Router,
    private authService: AuthorizationService,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map(r => {
        if (r instanceof HttpResponse) {
          if (r.status === 401) {
            const authError = r.body.message;
            this.handleAuthError(authError);
            return this.returnEmptyApiBody(r);
          }
        }
        return r;
      }),
      catchError(e => {
        if (e.status === 401) {
          const authError = e.message;
          this.handleAuthError(authError);
        } else {
          this.notificationService.notify('Something went wrong');
        }
        return of(null);
      }),
    );
  }

  private returnEmptyApiBody(r: HttpResponse<any>): HttpResponse<any> {
    return r.clone<any>({
      body: null,
    });
  }

  private handleAuthError(error: string) {
    this.router.navigate(['/login']);
    this.notificationService.notify(error);
    this.authService.eraseAuthInformation();
  }
}
