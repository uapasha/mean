import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class BackendUrlInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (req.url.startsWith('assets')) {
      return next.handle(req);
    }
    if (req.url.startsWith('http') || req.url.startsWith('https')) {
      return next.handle(req);
    }
    const reqToBackEnd = req.clone({
      url: environment.backEndApiUrl + req.url,
      params: req.params,
    });
    return next.handle(reqToBackEnd);
  }
}
