import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TaskListPage } from './pages/task-list/task-list-page.component';
import { TaskCreatePage } from './pages/task-create/task-create-page.component';
import { TaskEditPage } from './pages/task-edit/task-edit-page.component';
import { LoginPage } from './pages/login/login-page.component';
import {
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatGridListModule,
  MatInputModule,
  MatSnackBarConfig,
  MatSnackBarModule,
  MatToolbarModule,
} from '@angular/material';
import { CoreModule } from './core/core.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskComponent } from './pages/components/task/task.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskListPage,
    TaskCreatePage,
    TaskEditPage,
    LoginPage,
    TaskComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    CoreModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule,
    MatSnackBarModule,
  ],
  providers: [
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 5000,
        horizontalPosition: 'end',
        verticalPosition: 'bottom',
      } as MatSnackBarConfig,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
