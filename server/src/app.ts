import './env';
import { connectToDB } from './db';
import { startServer } from './server';
import { seedData } from './db/seedData';

connectToDB()
  .then(
    async (): Promise<void> => {
      if (process.env.SEED_DATA) {
        await seedData(true);
      }
      startServer();
    },
  )
  .catch(e => {
    // eslint-disable-next-line no-console
    console.error('error connecting to DB:');
    // eslint-disable-next-line no-console
    console.error(e);
  });
