import { Request } from 'express-serve-static-core';

export const handleApiRequest = (
  getResult: (req?: Request) => Promise<any>,
  handleError = null,
  method = 'json',
) => async (req, res): Promise<void> => {
  try {
    const result = await getResult(req);
    res[method](result);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    if (!handleError) {
      res.status(500).send(e.message || 'Something broke!');
    } else {
      handleError(req, res, e);
    }
  }
};
