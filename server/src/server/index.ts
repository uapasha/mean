import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';

import { handleApiRequest } from './utils';
import { User } from '../api/user';
import { API_URL, TASK_ENDPOINT, USER_ENDPOINT } from '../constants/core';
import { Task } from '../api/task';
import { authMiddleware } from './middlewares/auth';

const whitelist = ['http://localhost:4200'];
const corsOptions = {
  origin: (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

const app: express.Application = express();

app.use(cors(corsOptions));
app.use(helmet());
app.use(bodyParser.json());
app.use(authMiddleware);

app.get(
  `${API_URL}/${USER_ENDPOINT}`,
  handleApiRequest(async () => User.getAll()),
);

app.put(
  `${API_URL}/${USER_ENDPOINT}`,
  handleApiRequest(async ({ body }) => User.addOne(body)),
);

app.post(
  `${API_URL}/${USER_ENDPOINT}/login`,
  handleApiRequest(async ({ body }) => User.login(body)),
);

app.get(
  `${API_URL}/${TASK_ENDPOINT}/:userId`,
  handleApiRequest(async ({ params }) => Task.getByUser(params.userId)),
);

app.get(
  `${API_URL}/${TASK_ENDPOINT}/single/:_id`,
  handleApiRequest(async ({ params }) => Task.getById(params._id)),
);

app.put(
  `${API_URL}/${TASK_ENDPOINT}`,
  handleApiRequest(async ({ body }) => Task.addTask(body)),
);

app.delete(
  `${API_URL}/${TASK_ENDPOINT}/:_id`,
  handleApiRequest(({ params }) => Task.removeTask(params._id)),
);

app.post(
  `${API_URL}/${TASK_ENDPOINT}`,
  handleApiRequest(async ({ body }) => Task.editTask(body)),
);

export const startServer = () => {
  app.listen(3000, () => {
    // eslint-disable-next-line no-console
    console.log('App listening on port 3000!');
  });
};
