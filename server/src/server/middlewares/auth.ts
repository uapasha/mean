import { verifyToken } from '../../utils';
import { User } from '../../api/user';
import { API_URL, USER_ENDPOINT } from '../../constants/core';

const rejectWhenUserIsInvalid = res =>
  res.status(401).json({ message: 'User is not logged in' });

const isLogin = req =>
  req.url === `${API_URL}/${USER_ENDPOINT}/login` && req.method === 'POST';

const isNewUser = req =>
  req.url === `${API_URL}/${USER_ENDPOINT}` && req.method === 'PUT';

export const authMiddleware = async (req, res, next) => {
  if (isLogin(req) || isNewUser(req)) {
    return next();
  }
  try {
    const token = req.headers.authorization;
    const user = await verifyToken(token);
    if (user) {
      const userInDb = await User.getOne(user._id);
      if (userInDb) {
        req.user = userInDb;
        return next();
      }
      return rejectWhenUserIsInvalid(res);
    }
  } catch (e) {
    return rejectWhenUserIsInvalid(res);
  }
};
