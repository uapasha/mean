import { compare, hash } from 'bcryptjs';
import jwt from 'jsonwebtoken';

const SALT_LENGTH = 8;

export const getHash = (stringToHash: string): Promise<string> =>
  new Promise((resolve, reject) => {
    hash(stringToHash, SALT_LENGTH, (err, hash) =>
      err ? reject(err) : resolve(hash),
    );
  });

export const validateHash = (
  rawString: string,
  hash: string,
): Promise<boolean> => compare(rawString, hash);

export const signToken = (payload: any): Promise<string> =>
  new Promise((resolve, reject) => {
    jwt.sign(payload, process.env.JWT_SECRET, (err, token) =>
      err ? reject(err) : resolve(token),
    );
  });

export const verifyToken = (token: string): Promise<any> =>
  new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (err, token) =>
      err ? reject(err) : resolve(token),
    );
  });
