import { MongoClient } from 'mongodb';

const { MONGO_URI, DB_NAME } = process.env;

const client = new MongoClient(MONGO_URI, { useNewUrlParser: true });

const connectToDB = async (): Promise<MongoClient> => client.connect();

const getCollection = collectionName => {
  return client.db(DB_NAME).collection(collectionName);
};

export { connectToDB, getCollection };
