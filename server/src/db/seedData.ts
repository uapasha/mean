import { ObjectId } from 'bson';
import { getHash } from '../utils';
import { getCollection } from './index';

const getSeedData = async () => {
  const user1 = {
    _id: new ObjectId(),
    userName: 'user1',
    password: await getHash('12345678'),
  };
  const user2 = {
    _id: new ObjectId(),
    userName: 'user2',
    password: await getHash('87654321'),
  };

  const task1 = {
    userId: user1._id.toHexString(),
    description: 'First Task',
    dueDate: new Date('2019.09.01'),
  };

  const task2 = {
    userId: user1._id.toHexString(),
    description: 'Second Task',
    dueDate: new Date('2019.07.01'),
  };
  const task3 = {
    userId: user2._id.toHexString(),
    description: 'Only Task',
    dueDate: new Date('2019.07.01'),
  };

  return {
    users: [user1, user2],
    tasks: [task1, task2, task3],
  };
};

export const seedData = async (clearCollections?: boolean) => {
  const Tasks = getCollection('tasks');
  const Users = getCollection('users');
  const seed = await getSeedData();
  if (clearCollections) {
    await Tasks.deleteMany({});
    await Users.deleteMany({});
  }
  await Tasks.insertMany(seed.tasks);
  await Users.insertMany(seed.users);
};
