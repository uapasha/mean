export const API_URL = process.env.API_URL;
export const USER_ENDPOINT = 'user';
export const TASK_ENDPOINT = 'task';
