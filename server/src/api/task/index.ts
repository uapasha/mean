import { TaskModel } from './models';
import { getCollection } from '../../db';
import { Collection, ObjectId } from 'mongodb';

export class Task {
  public static get collection(): Collection {
    return getCollection('tasks');
  }

  public static async getByUser(userId: string): Promise<TaskModel[]> {
    return this.collection.find({ userId }).toArray();
  }

  public static async getById(_id): Promise<Task> {
    return this.collection.findOne({ _id: new ObjectId(_id) });
  }

  public static async addTask(task: TaskModel): Promise<TaskModel> {
    // todo @uapasha validate who can add the task
    const insertResult = await Task.collection.insertOne(task);
    return insertResult.ops[0];
  }

  public static async removeTask(_id): Promise<boolean> {
    await Task.collection.deleteOne({ _id: new ObjectId(_id) });
    return true;
  }

  public static async editTask({
    _id,
    ...newTask
  }: TaskModel): Promise<TaskModel> {
    return this.collection
      .findOneAndUpdate({ _id: new ObjectId(_id) }, { $set: newTask })
      .then(_ => _.value);
  }
}
