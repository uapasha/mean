import { ID } from '../../types';

export class TaskModel implements ID {
  public _id: string;
  public description: string;
  public userId: string;
}
