import { Collection, ObjectId } from 'mongodb';

import { UserModel } from './models';
import { getCollection } from '../../db';
import { getHash, signToken, validateHash } from '../../utils';

const validateNewUser = user => {
  if (typeof user.password !== 'string') {
    throw new Error('Please provide password');
  }
  if (typeof user.userName !== 'string') {
    throw new Error('Please provide user name');
  }
};

export class User {
  public static get collection(): Collection {
    return getCollection('users');
  }

  public static async addOne(user: any): Promise<UserModel> {
    validateNewUser(user);
    const passwordHash = await getHash(user.password);
    const insertRes = await this.collection.insertOne({
      ...user,
      password: passwordHash,
    });
    const insertedUser = insertRes.ops[0];
    delete insertedUser.password;
    return insertedUser;
  }

  public static async getOne(_id: string): Promise<UserModel> {
    return this.collection.findOne({ _id: new ObjectId(_id) });
  }

  public static async getAll(): Promise<UserModel[]> {
    return this.collection.find({}).toArray();
  }

  public static async login({
    password,
    userName,
  }: {
    password: string;
    userName: string;
  }): Promise<any> {
    const user = (await this.collection.findOne({ userName })) as UserModel;
    if (!user) {
      throw new Error('User not found');
    }
    const passwordIsValid = await validateHash(password, user.password);
    if (passwordIsValid) {
      delete user.password;
      const token = await signToken(user);
      return { token, userId: user._id, userName: user.userName };
    }
    throw new Error('Password is invalid');
  }
}
