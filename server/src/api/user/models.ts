import { ID } from '../../types';

export class UserModel implements ID {
  public _id: string;
  public userName: string;
  public password: string;
}
